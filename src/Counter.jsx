import React from "react";
import { useStore } from "./contextAPI/Practicse";

export const Counter = () => {
  const { state, dispatch } = useStore();
  return (
    <div>
      {state.count}
      <button
        onClick={() => dispatch({ type: "increment", message: "Incremented" })}
      >
        +
      </button>
      <button
        onClick={() => dispatch({ type: "decrement", message: "Decremented" })}
      >
        -
      </button>
      <button onClick={() => dispatch({ type: "reset", message: "Reset" })}>
        Reset
      </button>
      {state.message}
    </div>
  );
};
