import React from "react";

export const counterContext = React.createContext();
export const initialState = { count: 0, message: "" };

const reducer = (state, action) => {
  // eslint-disable-next-line default-case
  switch (action.type) {
    case "increment":
      return {
        count: state.count + 1,
        message: action.message
      };
    case "decrement":
      return {
        count: state.count - 1,
        message: action.message
      };
    case "reset":
      return {
        count: 0,
        message: action.message
      };
    default:
      throw new Error(`Unhandled action type: ${action.type}`);
  }
};

export const CounterProvider = ({ children }) => {
  const [state, dispatch] = React.useReducer(reducer, initialState);
  return (
    <counterContext.Provider value={{ state, dispatch }}>
      {children}
    </counterContext.Provider>
  );
};

export const useStore = () => React.useContext(counterContext);
