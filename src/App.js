import React from "react";
import { CounterProvider } from "./contextAPI/Practicse";
import { Counter } from "./Counter";

function App() {
  return (
    <>
      <CounterProvider>
        <Counter />
      </CounterProvider>
    </>
  );
}

export default App;
